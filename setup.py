import pathlib
from setuptools import setup, find_packages

# frm here:
# https://github.com/urswilke/miditapyr/ 
with open('requirements.txt') as f:
   requirements = f.read().splitlines()

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

setup(
    name='chaosmachine',
    version='0.0.1',
    description='translate temperature data from arduino serial connection to fluidsynth sound via midi',
    long_description=README,
    long_description_content_type="text/markdown",
    url='https://github.com/urswilke/miditapyr/',
    author='Urs Wilke',
    author_email='urs.wilke@gmail.com',
    license='MIT',
    packages=find_packages(exclude=['tests*']),
    # install_requires=find_packages(where='.'),
    install_requires=requirements,
    package_dir={'chaosmachine': 'chaosmachine'},
    # package_data={'miditapyr': ['data/*']},
    include_package_data=True,
    # data_files=[('recorded_data', ['serial_string_log.txt'])],
    package_data={'': ['recorded_data/*']},
    zip_safe=False)
