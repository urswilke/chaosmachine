# functions that are imported in the T_to_midi notebook
import itertools
import numpy as np
import mido 
import pandas as pd
import glob
import serial
import time
import datetime
import pkg_resources

def split_serial(serial_string):
    """Split strings sent by the serial conncection

    Args:
        serial_string (string): line printed to the serial conncection

    Returns:
        list: list of dictionaries containing the parts of the serial string
    """    

    temp_dicts = []
    # string_n = serial_string.decode()  # decode byte string into Unicode
    # serial_string = string_n.rstrip() # remove \n and \r

    # Run over all thermometers:
    thermo_parts = serial_string.split('; ')
    for thermo_part in thermo_parts:
        # create empty dictionary:
        temp_dict = dict()
        # split the sensor number and ID part from the temperature part:
        # (separated by ","):
        str_parts = thermo_part.split(', ')
        for s_part in str_parts:
            # split the part containing the key ("S" for "sensor", "ID" & "T" for "temperature")
            # from the part containing the value (after the ": "):
            s_parts = s_part.split(': ')
            d_part = dict([s_parts])
            # add the data to the dictionary:
            temp_dict.update(d_part)
        temp_dicts.append(temp_dict)
    return temp_dicts


def get_note_on_dict(diff_dicts, scales, root_at_0K, slopes, channels):
    """create list of dictionaries of note_on events

    Args:
        diff_dicts (list): list of dictionaries returned by derive_diffs
        scales (list): list of midi scales
        root_at_0K (list): list of y-intercepts
        slopes (list): list of slopes
        channels (list): list of channels

    Returns:
        list: list of dictionaries containing note_on data
    """
    note_on_dicts = []
    # print(len(diff_dicts))
    for i in range(len(diff_dicts)):
        thermo_dict = diff_dicts[i]
        note = get_closest_int(float(slopes[i] * thermo_dict['T']), scales[i], root_at_0K[i])
        # print(note_dict)
        # add 'type': 'note_on' to the dictionary:
        msg_dict = {'type': 'note_on', 'note': note, 'channel': channels[i]}
        note_on_dicts.append(msg_dict)

    return note_on_dicts

def get_note_off_dict(note_on_dicts):
    """[create list of dictionaries of note_off events

    Args:
        note_on_dicts (list): list of dictionaries containing note_on data

    Returns:
        list: list of dictionaries containing note_off data
    """
    note_off_dicts = []
    for note_on_dict in note_on_dicts:
        # replace 'type': by 'note_off' in the dictionary:
        note_off_dict = {**note_on_dict, **{'type': 'note_off'}}
        note_off_dicts.append(note_off_dict)
    return note_off_dicts



def get_closest_int(floatval, int_list, root_at_0K):
    """return closest integer value of list

    Args:
        floatval (float): value to look for
        int_list (list of integers): list to compare with
        root_at_0K ([type]): y-intercept

    Returns:
        int: list value closest to floatval
    """
    # from here: https://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value
    a = min(int_list, key=lambda x:abs(x - (floatval + root_at_0K)))
    return a


def derive_diffs(temp_dicts):
    # https://stackoverflow.com/questions/40092474/what-is-the-fastest-way-to-get-all-pairwise-combinations-in-an-array-in-python/40092582
    tuple_list = list(itertools.combinations(temp_dicts, 2))
    name_list = ['S' + i[0]['S'] + '-' + i[1]['S'] for i in tuple_list]
    diff_list = [round(float(i[0]['T']) - float(i[1]['T']), 4) for i in tuple_list]
    # https://stackoverflow.com/questions/54849211/create-list-of-dictionaries-from-two-lists-using-for-loop-in-python
    diff_dicts = []
    for i, j in zip(name_list, diff_list):
        diff_dicts.append({"S": i, "T": j})

    return diff_dicts


def get_chord_scale_notes(chord):
    """generate midi notes from pitch classes

    Args:
        chord (list of integers): pitch classes

    Returns:
        list: list of scale midi notes
    """
    octaves = np.array([12 * (j // len(chord)) for j in range(len(chord) * 11)])

    scale_pitch = np.array(chord * 11)

    chord_midi_notes = octaves + scale_pitch
    chord_midi_notes = chord_midi_notes[chord_midi_notes <= 127]
    chord_midi_notes = list(chord_midi_notes)
    return chord_midi_notes


def midi_port_to_file(msg_times, msg_data, filename):
    """save chaos machine data to midi file

    Args:
        msg_times (list): list of midi message times
        msg_data (list): list of midi messages
        filename (string): name of the midi file
    """
    df = pd.DataFrame({'time': msg_times, 'data': msg_data}).explode('data').dropna()
    # exploding creates repeated indices for events occurring at the same time.
    # -> create ascending indices:
    df.reset_index(inplace=True)
    
    # create data frame of events' data:
    l2 = []
    for idx in range(len(df)):
        l2.append({**vars(df.data[idx])})
    df_events = pd.DataFrame(l2).drop('time', axis=1)
    df = pd.concat([df, df_events], axis=1)

    # https://stackoverflow.com/questions/22923775/calculate-pandas-dataframe-time-difference-between-two-columns-in-hours-and-minu
    df['time_diff'] = (pd.to_datetime(df['time']) - pd.to_datetime(df.groupby(['channel'])['time'].shift(1))).astype('timedelta64[ms]').fillna(0)

    # first element is nan ...:
    # df.loc[0, 'time_diff'] = 0
    df.time_diff = df.time_diff.astype('int')
    # replace time values in event data by the time increments recorded:
    l = []
    for idx in range(len(df)):
        l.append({**vars(df.data[idx]), **{'time': df.time_diff[idx]}})
    df['midi'] = l

    # create midi file:
    tracks = df_events.channel.unique()

    mid = mido.MidiFile()
    l_track = []
    for i in tracks:
        l_track.append(mido.MidiTrack())
        mid.tracks.append(l_track[i])
    for idx in range(len(df)):
        i_track = df_events['channel'][idx]
        l_track[i_track].append(mido.Message(**df.midi[idx]))
    mid.save(filename)

def connect_fluidport(fluidport_string = "FLUID Synth", in_or_or_fun = mido.get_output_names):
    """connect to output port containing substring

    Args:
        fluidport_string (str, optional): Substring to look for. Defaults to "FLUID Synth".
        in_or_or_fun (function, optional): [description]. Defaults to mido.get_output_names.

    Returns:
        [type]: [description]
    """
    all_outports = in_or_or_fun()
    print('all outputs detected: ', all_outports)

    fluidport = [s for s in all_outports if fluidport_string in s]
    print('port chosen: ', fluidport)


    return mido.open_output(fluidport[0])

def connect_microcontroller_serial(port_path = '/dev/ttyACM*'):
    """connect to serial connection of midi controller

    Args:
        port_path (str, optional): Path where midicontroller is mounted. Defaults to '/dev/ttyACM*'.

    Returns:
        mido outport: object created by serial.Serial() 
    """
    dev_port = glob.glob(port_path)[0]
    print(dev_port)
    serial_port = serial.Serial(dev_port, 9600)

    time.sleep(0.5)
    return serial_port

def get_thermometer_ids(serial_port):
    """get thermometer ids of serial

    Args:
        serial_port (serial port): returned by connect_microcontroller_serial()

    Returns:
        list: list of id strings
    """
    serial_string = serial_port.readline().decode().rstrip()
    print('string received: ', serial_string)
    return [i['ID'] for i in split_serial(serial_string)]


def chaos_machine(
    n_total_timesteps = 50,
    serial_port = None,
    demo_mode = False,
    demo_mode_time_step = 1,
    midi_inport = None,
    n_thermos = None,
    outport = None,
    n_diffs = None,
    scales = None,
    root_at_0K = None,
    slopes = None,
    channels = None,
    write_csvs = False,
    write_midifile = False,
    midi_filename = 'live_record.mid',
    filepath = None,
    live_mode = True,
    verbose = True
):
    """run the chaos machine

    Args:
        n_total_timesteps (int, optional): number of time steps. Defaults to 30.
        serial_port (serial port, optional): serial port to the midicontroller. Defaults to connect_microcontroller_serial().
        midi_inport (port to fluidsynth, optional): mido port to midicontroller. Defaults to None.
        n_thermos ([type], optional): number of thermometers. Defaults to None.
        outport (port to fluidsynth, optional): mido port to fluidsynth. Defaults to connect_fluidport().
        n_diffs (int, optional): number of thermometer combinations. Defaults to None.
        scales (list, optional): list of sales. Defaults to None.
        root_at_0K (list, optional): list of y-intercepts. Defaults to None.
        slopes (list, optional): list of slopes. Defaults to None.
        channels (list, optional): list of channels. Defaults to None.
        verbose (bool, optional): print out messages when running? Defaults to True.

    Returns:
        tuple: all loop data 
    """
    if outport is None and live_mode == True:
        outport = connect_fluidport()
    if serial_port is None and demo_mode == False:
        serial_port = connect_microcontroller_serial()

    # assign default values if None:
    if demo_mode == False:
        if n_thermos is None:
            n_thermos = len(get_thermometer_ids(serial_port))
        if n_diffs is None:
            n_diffs = int((n_thermos - 1) * n_thermos / 2)
        # Flush serial buffer:
        serial_port.flushInput()
        # read_new_line = pretty_print_serial_line
        demo_mode_time_step = 0

    else:
        n_thermos = 5
        n_diffs = 10
        serial_file = pkg_resources.resource_stream(__name__, 'recorded_data/serial_string_log.txt')
        # serial_file = open('chaosmachine/recorded_data/serial_string_log.txt')
        # read_new_line = serial_file.readline

    if live_mode == False:
        demo_mode_time_step = 0
    if scales is None:
        chord = []
        if midi_inport is not None:
            chord = get_midi_scale_from_controller(midi_inport)


        if len(chord) == 0:
        # else:
            chord = [0, 2, 4, 5, 7, 9, 11]

        scale_midi_notes = get_chord_scale_notes(chord)
        print(scale_midi_notes)
        scales = [scale_midi_notes] * n_diffs
    if root_at_0K is None:
        root_at_0K = [60] * n_diffs
    if slopes is None:
        slopes = [2] * n_diffs
    if channels is None:
        channels = list(range(n_diffs))
    
    # empty lists to store the data:
    times = []
    msg_times = []
    midi_data =[]
    temp_data =[]
    diff_data =[]
    msg_data =[]
    serial_data =[]
    # lists of dicts (initialized empty) to compare the notes of the previous time step:
    previous_note_dicts = [dict()] * n_diffs

    # Wait 100 ms:
    time.sleep(0.1)






    # Run over all timesteps:
    for i in range(n_total_timesteps):
        if live_mode == True:
            timeval = datetime.datetime.now().time().isoformat(timespec='milliseconds')
        else:
            timeval = (datetime.datetime.now() + datetime.timedelta(seconds=i)).time().isoformat(timespec='milliseconds')
        if demo_mode == True:
            serial_string = serial_file.readline().decode().rstrip()
        else:
            serial_string = pretty_print_serial_line(serial_port)
        print(serial_string)
        serial_data.append(serial_string)
        # calculate live loop objects:
        temp_dicts = split_serial(serial_string)
        diff_dicts = derive_diffs(temp_dicts)
        note_on_dicts = get_note_on_dict(diff_dicts, scales, root_at_0K, slopes, channels)
        #record measurements:
        times.append(timeval)
        temp_data.append(temp_dicts)
        diff_data.append(diff_dicts)
        midi_data.append(note_on_dicts)
        if verbose == True:
            # live print:
            print('T:    ', [d['T'] for d in diff_dicts])
            print('note: ', [d['note'] for d in note_on_dicts])
        msgs = []
        # midi events:
        for j in range(len(note_on_dicts)):
            if previous_note_dicts[j] != note_on_dicts[j] or i == n_total_timesteps - 1:
                if i > 0 or i == n_total_timesteps - 1:
                    msg = mido.Message(**note_off_dicts[j])
                    if live_mode == True:
                        outport.send(msg)
                    msgs.append(msg)
                if i < n_total_timesteps - 1:
                    msg = mido.Message(**note_on_dicts[j])
                    if live_mode == True:
                        outport.send(msg)
                    msgs.append(msg)
        msg_data.append(msgs)
        msg_times.append(timeval)
        previous_note_dicts = note_on_dicts
        note_off_dicts = get_note_off_dict(note_on_dicts)
        time.sleep(demo_mode_time_step)

    if write_csvs == True:
        save_csvs(times, temp_data, diff_data, midi_data, n_thermos, diff_dicts, filepath)
    if write_midifile == True:
        midi_port_to_file(msg_times, msg_data, filepath + midi_filename)

    return times, msg_times, midi_data, temp_data, diff_data, msg_data, serial_data


def get_midi_scale_from_controller(midi_inport):
    """return list of pitch notes played on the midi controller

    Args:
        midi_inport (mido port): midi controller mido port

    Returns:
        list: list of pitch class integers
    """
    midi_key_input = []
    print("Be quick! You've 5 seconds to enter scale keys or take C major")
    t_end = time.time() + 5
    while time.time() < t_end:
        time.sleep(0.01)
        msg = midi_inport.receive()
        midi_key_input.append(msg.note)
        print(midi_key_input)

    all_unique_input_notes=list(set(midi_key_input))
    print(all_unique_input_notes)
    # get chord pitches (from 0 to 11):
    chord = list(np.sort(np.array(all_unique_input_notes)%12))
    print('pitches entered:', chord)
    return chord

def pretty_print_serial_line(serial_port):
    return serial_port.readline().decode().rstrip()
    
def save_csvs(times, temp_data, diff_data, midi_data, n_thermos, diff_dicts, filepath):
    df = pd.DataFrame(temp_data)
    df.columns = ['T' + str(l+1) for l in list(range(n_thermos))]
    df['time'] = times
    df.to_csv(filepath + 'temp_data_recording.csv', index=False)
    df = pd.DataFrame(diff_data)
    df.columns = [d['S'] for d in diff_dicts]
    df['time'] = times
    df.to_csv(filepath + 'diff_data_recording.csv', index=False)
    df = pd.DataFrame(midi_data)
    df.columns = [d['S'] for d in diff_dicts]
    df['time'] = times
    df.to_csv(filepath + 'midi_data_recording.csv', index=False)    