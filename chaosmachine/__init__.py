import itertools
import numpy as np
from mido import Message, MidiFile, MidiTrack
import pandas as pd
from .chaos_functions import split_serial, get_note_on_dict, get_note_off_dict, get_closest_int, derive_diffs, get_chord_scale_notes, midi_port_to_file, connect_fluidport, connect_microcontroller_serial, get_thermometer_ids, chaos_machine

