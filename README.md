# Chaos Machine


package for the project https://gitlab.com/urswilke/chaos_machine_code/

## Installation

install the package by entering:
``` bash
pip install git+https://gitlab.com/urswilke/chaosmachine.git
```
in your virtual environment. You also need to install fluidsynth on your computer (for ubuntu see [here](https://zoomadmin.com/HowToInstall/UbuntuPackage/qsynth)).


## Running the chaos machine

### Default parameters

Start fluidsynth and connect your midicontroller to the PC (wait a few seconds), 
and then run the chaos machine (start python from your terminal and then enter these commands to the prompt):

```python
import chaosmachine
import mido

# run with default parameters:
chaosmachine.chaos_machine()
```

### Return values

The function returns the follwing objects:

```python
times, msg_times, midi_data, temp_data, diff_data, msg_data, serial_data = chaosmachine.chaos_machine()
```

### Demo mode

If you don't have the microcontroller and the thermometers, you can also run the chaos machine in demo mode, where the temperature data is read from a data file included in the package. In this case you just need to install fluidsynth.

```python
chaosmachine.chaos_machine(demo_mode=True)
```



### Midi controller input

You can also use a midi controller input for the scale the machine plays in:

```python
# find midi controller midi port with:
mido.get_input_names()
# look in the output and
# replace the following string with the midicontroller you want from these options on your machine:
midi_inport = mido.open_input('Virtual Keyboard:Virtual Keyboard 136:0')

chaosmachine.chaos_machine(midi_inport = midi_inport)

```


### Midifile output

In the `demo_mode` you can also set `live_mode = False`, if you're only interested in the midi file output:

```python
chaosmachine.chaos_machine(
    n_total_timesteps=10, 
    filepath='chaosmachine/recorded_data/', 
    live_mode=False, 
    demo_mode=True, 
    write_midifile = True,
    midi_filename='live_record.mid'
)
```

In this way the machine runs much quicker as it doesn't have to wait for the next note_on or note_off events to arrive in the `live_mode` loop.

By choosing `write_midifile = True` we can write a midi file.
In this way, for example you can create the midi files for different scales at once:


### Generating multiple midifiles

If you want to fine-tune the input parameters [scales, slopes and y-intercepts](https://gitlab.com/urswilke/chaos_machine_code/-/blob/master/R/midi_intro/midi_intro.md) of the machine, it might be helpfull to be able to produce multiple audio files by just changing some parameters and keeping the rest the same (that you don't have to restart the machine for each manually).

What about using the default parameters except for the scale:

```python
# define three scales objects (each one is defined for all 10 midi channels):
cmaj = [chaosmachine.get_chord_scale_notes([0, 2, 4, 5, 7, 9, 11])] * 10
chromatic = [list(range(128))] * 10
cmajchord = [chaosmachine.get_chord_scale_notes([0, 4, 7])] * 10

scale_list = [cmaj, chromatic, cmajchord]


# save the results to 3 resulting midi files:
import subprocess
mypath = 'chaosmachine/recorded_data/'
for i_scale in range(len(scale_list)):
    file_prefix = 'live_record' + str(i_scale)
    chaosmachine.chaos_machine(
        scales = scale_list[i_scale], 
        n_total_timesteps=10, 
        filepath=mypath, 
        live_mode=False, 
        demo_mode=True,
        write_midifile = True,
        midi_filename=file_prefix + '.mid'
    )
    # Synthesize midi to wav using fluidsynth and sf2 soundfont:
    subprocess.run([
        "fluidsynth", 
        "/home/chief/soundfonts/KBH_Real_and_Swell_Choir.sf2", 
        "-F",
        mypath + file_prefix + '.wav',
        mypath + file_prefix + '.mid'
    ])
    # compress wav to mp3:
    subprocess.run([
        'lame', 
        mypath + file_prefix + '.wav', 
        mypath + file_prefix + '.mp3'
    ])    
```

This writes the 3 files live_record0.mid, live_record0.mid and live_record0.mid with notes in the 3 respective scales. And the synthesized wav and mp3 files. Except the wav, the [resulting files](chaosmachine/recorded_data/) can be found in this repository.


